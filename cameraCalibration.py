import cv2
import numpy as np

def computeCameraPosition(objectPointsPath, imagePointsPath):
    objectPoints = np.genfromtxt(objectPointsPath, dtype='float32', delimiter='\t')
    imagePoints  = np.genfromtxt(imagePointsPath,  dtype='float32', delimiter='\t')
    cameraMatrix = np.array([[20, 0 , 200], [0, 20, 200], [0, 0, 1]])
    return cv2.calibrateCamera([objectPoints], 
            [imagePoints], 
            (400, 400), 
            cameraMatrix, 
            flags=cv2.CALIB_USE_INTRINSIC_GUESS)

if __name__ == "__main__":
    print(computeCameraPosition('exampleData/3D_points.txt', 'exampleData/2D_points.txt'))
